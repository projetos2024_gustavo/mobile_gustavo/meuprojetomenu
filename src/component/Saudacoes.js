import { StatusBar } from "expo-status-bar";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";

function Saudacoes() {
  return (
    <View style={styles.container}>
      <Text>Olá mundo</Text>
      <Text>Hello World</Text>
      <Text>Hallo Welt</Text>
      <Text>Bonjour le monde</Text>
    </View>
  );
}

export default Saudacoes;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },

  teste: {
    padding: 10,
    margin: 5,
    backgroundColor: "lightblue",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
  },
});