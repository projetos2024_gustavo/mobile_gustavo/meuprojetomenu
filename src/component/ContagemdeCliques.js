import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    TouchableOpacity,
    Button,
  } from "react-native";
  import { useState } from "react";
  
  function ContagemdeCliques() {
    const [count, setCount] = useState(0);
  
    return (
      <View style={styles.container}>
        <ScrollView>
          <Text style={styles.teste}>Clique para contar:{count}</Text>
          <TouchableOpacity onPress={() => setCount(count + 1)}
          style={styles.teste}>
            <Text>Clique!</Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
    },
  
    teste: {
      padding: 10,
      margin: 5,
      backgroundColor: "lightblue",
      borderRadius: 5,
      alignItems: "center",
      justifyContent: "center",
    },
  });
  export default ContagemdeCliques;
  