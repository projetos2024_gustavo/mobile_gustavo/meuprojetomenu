import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import ContagemdeCaracteres from "./src/component/ContagemdeCaracteres";
import ContagemdeCliques from "./src/component/ContagemdeCliques";
import Menu from "./src/component/Menu";
import Saudacoes from "./src/component/Saudacoes";
import Botao from "./src/component/Botao";
import ListadeTexto from "./src/component/ListadeTextos";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Menu" component={Menu} />
        <Stack.Screen name="ContagemdeCliques" component={ContagemdeCliques} />
        <Stack.Screen
          name="ContagemdeCaracteres"
          component={ContagemdeCaracteres}
        />
        <Stack.Screen name="Saudacoes" component={Saudacoes} />
        <Stack.Screen name="Botao" component={Botao} />
        <Stack.Screen name="ListadeTexto" component={ListadeTexto} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
